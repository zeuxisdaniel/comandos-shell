Comandos varios de ayuda para Arq. de Servicios
====================

## Actualizar

Solo para entornos debian. Realiza apt update y apt upgrade.

## Conectar SSH

Enlista y realiza conexión ssh hacia equipo ASO seleccionado.

## VPN

Desactiva ipv6 para realizar conexión VPN hacia red del banco.

## Copiar SSH ID

Realiza una copia de la llave ssh local hacia los equipos especificados para que se confíe en ella.

## Setear BASH

Setea BASH como interprete principal en los equipo especificados.

## Licencia IntelliJ

Restaura con nueva fecha la licencia de IntelliJ para extender periodo de prueba.

## Servicios Monitor

Busca SMC's de servicios en los diferentes monitores de los países y entornos.